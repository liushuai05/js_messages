  //生成Html
  var GenerateHtml = function (type, title, msg) {
    var _html = "";
    _html += '<div id="mb_box"></div><div id="mb_con"><span id="mb_tit">' + title + '</span>';

    _html += '<!--<a id="mb_ico">x</a>--><div id="mb_msg">' + msg + '</div><div id="mb_btnbox">';

 

    if (type == "alert") {

      _html += '<input id="mb_btn_ok" type="button" value="确定" />';

    }

    if (type == "confirm") {

      _html += '<input id="mb_btn_ok" type="button" value="确定" />';

      _html += '<input id="mb_btn_no" type="button" value="取消" />';

    }

    _html += '</div></div>';

 

    //必须先将_html添加到body，再设置Css样式

    $("body").append(_html); GenerateCss();

    // $().fadeIn(500);

    // $('#mb_con,#mb_box').fadeIn(500);
    $('#mb_con,#mb_box').show()
  }

 

  //生成Css

  var GenerateCss = function () {

 

    $("#mb_box").css({ width: '100%', height: '100%', zIndex: '99999', position: 'fixed',

      filter: 'Alpha(opacity=60)', backgroundColor: 'black', top: '0', left: '0', opacity: '0.6'

    });

 

    $("#mb_con").css({ zIndex: '999999', width: '300px',display: 'none', position: 'fixed',

      backgroundColor: 'White', borderRadius: '10px'

    });

 

    $("#mb_tit").css({ display: 'block', fontSize: '14px', color: '#444', padding: '10px 15px',

      backgroundColor: '#DDD', borderRadius: '10px 10px 0 0',

      borderBottom: '3px solid #009BFE', fontWeight: 'bold'

    });

 

    $("#mb_msg").css({ padding: '20px', lineHeight: '20px',

      borderBottom: '1px dashed #DDD', fontSize: '13px'

    });

 

    $("#mb_ico").css({ display: 'block', position: 'absolute', right: '10px', top: '9px',

      border: '1px solid Gray', width: '18px', height: '18px', textAlign: 'center',

      lineHeight: '16px', cursor: 'pointer', borderRadius: '12px', fontFamily: '微软雅黑'

    });

 

    $("#mb_btnbox").css({ margin: '15px 0 10px 0', textAlign: 'center' });

    $("#mb_btn_ok,#mb_btn_no").css({ width: '85px', height: '30px', color: 'white', border: 'none'  });

    $("#mb_btn_ok").css({ backgroundColor: '#168bbb' ,borderRadius:'5px' });

    $("#mb_btn_no").css({ backgroundColor: 'red', marginLeft: '20px' ,borderRadius:'5px'  });

 

 

    //右上角关闭按钮hover样式

    // $("#mb_ico").hover(function () {

    //   $(this).css({ backgroundColor: 'Red', color: 'White' });

    // }, function () {

    //   $(this).css({ backgroundColor: '#DDD', color: 'black' });

    // });

 

    var _widht = document.documentElement.clientWidth; //屏幕宽

    var _height = document.documentElement.clientHeight; //屏幕高

 

    var boxWidth = $("#mb_con").width();

    var boxHeight = $("#mb_con").height();

 

    //让提示框居中

    // $("#mb_con").css({ top: (_height - boxHeight) / 2 + "px", left: (_widht - boxWidth) / 2 + "px" });
     $("#mb_con").css({ top: (_height - boxHeight) / 2 + "px", left:  " calc( ( 100% - 300px ) / 2 ) " });

  }

 

 

  //确定按钮事件

  var btnOk = function (callback) {

    $("#mb_btn_ok").click(function () {

    // $('#mb_con,#mb_box').fadeOut(500);
    $('#mb_con,#mb_box').hide()
    setTimeout('$("#mb_box,#mb_con").remove()',10);
    // setTimeout('$("#mb_box,#mb_con").remove()',500);

      

      if (typeof (callback) == 'function') {

        callback();

      }

    });

  }

 

  //取消按钮事件

  var btnNo = function () {

    $("#mb_btn_no,#mb_ico").click(function () {

	    // $('#mb_con,#mb_box').fadeOut(500);
      $('#mb_con,#mb_box').hide()

	    // setTimeout('$("#mb_box,#mb_con").remove()',500);
	    setTimeout('$("#mb_box,#mb_con").remove()',10);

    });

  }


extend_obj={
  alerts:function (title, msg) {

    GenerateHtml("alert", title, msg);

    btnOk(); //alert只是弹出消息，因此没必要用到回调函数callback

    btnNo();

  },

confirms: function (title, msg, callback) {

    GenerateHtml("confirm", title, msg);

    if(btnOk(callback)){

      alert(4554);

    }

    btnNo();

  },
tip:function(msg, position, duration) {

          if(!msg){
              var m=document.getElementById('core_show_div');
                  var d = 0.2;
                  m.style.webkitTransition = '-webkit-transform ' + d + 's ease-in, opacity ' + d + 's ease-in';
                  m.style.opacity = '0';
                  setTimeout(function() {
                      document.body.removeChild(m)
                  }, d * 1000);
                  return;
          }
         if(position!='bottom' && position!='middle' && position!='top'){
             position ='bottom';
         }
         
          duration = isNaN(duration) ? 1000 : duration;
          var m = document.createElement('div');
          m.id = 'core_show_div';
          m.innerHTML = msg;
          var css = "width:60%; font-size:14px;min-width:150px; background:#000; opacity:0.7; min-height:35px; overflow:hidden; color:#fff; line-height:35px; text-align:center; border-radius:5px; position:fixed; left:20%; z-index:999999;box-shadow:3px 3px 4px #d9d9d9;-webkit-box-shadow: 3px 3px 4px #d9d9d9;-moz-box-shadow: 3px 3px 4px #d9d9d9;";
          if(position=='top'){
              css+="top:10%; ";
          } else if(position=='bottom'){
               css+="bottom:10%; ";
          } else if(position=='middle'){
               css+="top:50%;margin-top:-18px;";
          }
          m.style.cssText = css;
          document.body.appendChild(m);
          if(duration!=0){
              setTimeout(function() {
                  var d = 0.2;
                  m.style.webkitTransition = '-webkit-transform ' + d + 's ease-in, opacity ' + d + 's ease-in';
                  m.style.opacity = '0';
                  setTimeout(function() {
                      document.body.removeChild(m)
                  }, d * 1000);
              }, duration);
          }

      }
}


if($.fn&&$.fn.jquery){
;(function () {
  $.extend(extend_obj)
})(jQuery);
}else{
  ;(function () {
    $.extend($, extend_obj)
  })(Zepto);
}